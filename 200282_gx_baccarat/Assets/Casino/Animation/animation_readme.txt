
About Card Animation

1) Attach an Animator component to the card you want to animate.
2) Create an Animator Controller in the Project tab.
3) With the Animator selected, drag and drop the animation you want to use from the Animation folder in the Project tab onto the graph in the Animator tab.
4) If you want to edit the animation or create a new one, you can do so with PCs1 selected and the Dopesheet in the Animation tab. 
   If this is not possible, attach the Animator of PCs1 in the Animation folder to PCs1 in the Overview map.
5) Check Apply Root Motion in the Animator component if you want the animation to play in a different position. 
   If the rotation is wrong, please align the rotation value (control the rotation.z value programmatically or edit it in the Animation tab).