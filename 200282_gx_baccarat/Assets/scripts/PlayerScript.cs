﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // this used for both player and banker

    //get other scripts
    public CardScript cardScript;
    public DeckScript deckScript;

    //total value of player/banker hands
    public int handValue = 0;
    public int adjustedHandValue = -10;

    //betting money
    private int money = 500;

    //array of cards on the table
    public GameObject[] hand;

    //index of next card to turn over
    public int cardIndex = 0;


    public void StartHand()
    {
        GetCard();
        GetCard();
        StartCoroutine(Get3rdCardCheck());
        AdjustHandValue();//adjust final hand values


    }

    //add cards to hand
    public int GetCard()
    {
        //get a card using deal card. this assigns card sprites and values to cards on table
        int cardValues = deckScript.DealCard(hand[cardIndex].GetComponent<CardScript>());
        // Show card on the game screen
        hand[cardIndex].GetComponent<Renderer>().enabled = true;
        // Add card value to current total of hand
        handValue += cardValues;
        cardIndex++;
        return handValue;


    }

    public IEnumerator Get3rdCardCheck()
    {
        yield return new WaitForSeconds(1);
        Get3rdCard();

    }

    public void Get3rdCard()
    {
        //if hand score isn't 9 or 8 for either banker or player, give 3rd card
        if (handValue != 8 && handValue != 9)
        {
            GameObject.Find("Player").GetComponent<PlayerScript>().PlayerGets3rdCard();
            GameObject.Find("Banker").GetComponent<PlayerScript>().BankerGets3rdCard();
        }
    }
    public void PlayerGets3rdCard()
    {
        //if player's hand is between 0 and 5, player gets 3rd card
        if (handValue >= 0 && handValue <= 5)
        {
            GetCard();

        }
    }
    private void BankerGets3rdCard()
    {
        //if banker's hand is between 0 and 2, banker gets 3rd card
        if (handValue >= 0 && handValue <= 2)
        {
            GetCard();
            
        }
    }
    //if hand is over 9 remove 10 for adjusted score
    public void AdjustHandValue()
    {
        if (handValue > 9)
        {
            handValue = handValue - 10;
            GameObject.Find("GameManger").GetComponent<GameManager>().playerScoreText.text = handValue.ToString();
            GameObject.Find("GameManger").GetComponent<GameManager>().bankerScoreText.text = handValue.ToString();
        }
    }

    // Add or subtract from money, for bets
    public void AdjustMoney(int amount)
    {
        money += amount;
    }

    // show players current amount of money
    public int GetMoney()
    {
        return money;
    }
    //resets all relevant variables and hides cards again
    public void ResetHand()
    {
        for (int i = 0; i < hand.Length; i++)
        {
            hand[i].GetComponent<CardScript>().ResetCard();
            hand[i].GetComponent<Renderer>().enabled = false;
        }
        cardIndex = 0;
        handValue = 0;


    }
}
