﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Game Buttons
    public Button betBtn;
    public Button dealBtn;
    //chip buttons
    public Button chip1Btn;
    public Button chip5Btn;
    public Button chip10Btn;
    public Button chip20Btn;
    public Button chip50Btn;
    //bet area buttons
    public Button playerBtn;
    public Button bankerBtn;
    public Button tieBtn;
    public Button exitBtn;


    //access player/dealer script
    public PlayerScript playerScript;
    public PlayerScript bankerScript;

    //bet button group
    [SerializeField] GameObject buttonGroup1;
    [SerializeField] GameObject buttonGroup2;
    [SerializeField] GameObject playerHandValues;
    [SerializeField] GameObject bankerHandValues;
    [SerializeField] GameObject loser;
    [SerializeField] GameObject winner;

    //HUD text access and updates
    public Text playerScoreText;
    public Text bankerScoreText;
    public Text betsText;
    public Text cashText;
    public Text youWinText;
    public Text betOnText;

    //how much is bet
    int pot = 0;

    //bet values
    int chip1 = 1;
    int chip5 = 5;
    int chip10 = 10;
    int chip20 = 20;
    int chip50 = 50;

    //win results
    bool bankerWins = false;
    bool playerWins = false;
    bool tieWins = false;
    bool roundOver = false;
    bool winBet = false;
    bool loseBet = false;
    private void Start()
    {
        //click listeners for buttons
        betBtn.onClick.AddListener(() => BetClicked());
        dealBtn.onClick.AddListener(() => DealClicked());
        //chips
        chip1Btn.onClick.AddListener(() => Chip1Clicked());
        chip5Btn.onClick.AddListener(() => Chip5Clicked());
        chip10Btn.onClick.AddListener(() => Chip10Clicked());
        chip20Btn.onClick.AddListener(() => Chip20Clicked());
        chip50Btn.onClick.AddListener(() => Chip50Clicked());
        //bet areas
        playerBtn.onClick.AddListener(() => PlayerBetAreaClicked());
        bankerBtn.onClick.AddListener(() => BankerBetAreaClicked());
        tieBtn.onClick.AddListener(() => TieBetAreaClicked());
        exitBtn.onClick.AddListener(() => ExitApp());

    }


    //bet button enables/disables chip buttons
    private void BetClicked()
    {
        //if bet button is active, keep bet options deactivated
        if (buttonGroup1.activeSelf)
        {
            buttonGroup1.SetActive(false);

        }
        //else if bet button is pressed, reveal chip buttons
        else
        {
            buttonGroup1.SetActive(true);
        }
        //if bet button is active, keep chip options deactivated
        if (buttonGroup2.activeSelf)
        {
            buttonGroup2.SetActive(false);
        }
        //else if bet button is pressed, reveal bet area buttons
        else
        {
            buttonGroup2.SetActive(true);
        }
    }

    private void Chip1Clicked()
    {
        //make bet
        pot = pot + 1;
        //take bet from cash
        playerScript.AdjustMoney(-1);

        //bet text update
        betsText.text = "Bet: $" + pot.ToString();

        cashText.text = "Cash: $" + playerScript.GetMoney().ToString();
    }
    private void Chip5Clicked()
    {
        //make bet
        pot = pot + 5;
        //take bet amount from cash
        playerScript.AdjustMoney(-5);

        //bet text update
        betsText.text = "Bet: $" + pot.ToString();

        cashText.text = "Cash: $" + playerScript.GetMoney().ToString();
    }
    private void Chip10Clicked()
    {
        //make bet
        pot = pot + 10;
        //take bet amount from cash
        playerScript.AdjustMoney(-10);

        //bet text update
        betsText.text = "Bet: $" + pot.ToString();

        cashText.text = "Cash: $" + playerScript.GetMoney().ToString();
    }
    private void Chip20Clicked()
    {
        //Make bet
        pot = pot + 20;
        //take bet from cash
        playerScript.AdjustMoney(-20);

        //bet text update
        betsText.text = "Bet: $" + pot.ToString();

        cashText.text = "Cash: $" + playerScript.GetMoney().ToString();
    }
    private void Chip50Clicked()
    {
        //make bet
        pot = pot + 50;
        //take bet from cash
        playerScript.AdjustMoney(-50);

        //bet text update
        betsText.text = "Bet: $" + pot.ToString();

        cashText.text = "Cash: $" + playerScript.GetMoney().ToString();
    }



    private void PlayerBetAreaClicked()
    {
        playerWins = true;
        betOnText.text = "Bet on: Player";
        dealBtn.interactable = true;
        dealBtn.gameObject.SetActive(true);
    }
    private void BankerBetAreaClicked()
    {
        bankerWins = true;
        betOnText.text = "Bet on: Banker";
        dealBtn.interactable = true;
        dealBtn.gameObject.SetActive(true);
    }
    private void TieBetAreaClicked()
    {
        tieWins = true;
        buttonGroup2.SetActive(false);
        betOnText.text = "Bet on: Tie";
        dealBtn.interactable = true;
        dealBtn.gameObject.SetActive(true);
    }

    private void DealClicked()
    {
        GameObject.Find("deck").GetComponent<DeckScript>().Shuffle();
        playerScript.StartHand();
        Debug.Log("player hand dealt");
        bankerScript.StartHand();
        Debug.Log("banker hand dealt");
        //reveal hand values
        playerHandValues.SetActive(true);
        bankerHandValues.SetActive(true);
        //update hand values
        playerScoreText.text = playerScript.handValue.ToString();
        bankerScoreText.text = bankerScript.handValue.ToString();
        //button visability
        dealBtn.gameObject.SetActive(false);
        betBtn.gameObject.SetActive(false);
        buttonGroup1.SetActive(false);
        buttonGroup2.SetActive(false);
        StartCoroutine(Results());
    }

    IEnumerator Results()
    {
        playerScript.AdjustHandValue();//adjust final hand values
        yield return new WaitForSeconds(3);
        HandResults();
    }

    private void HandResults()
    {

        //player wins and correct bet made money
        if (GameObject.Find("Player").GetComponent<PlayerScript>().handValue > GameObject.Find("Banker").GetComponent<PlayerScript>().handValue)
        {
            if (playerWins == true)
            {
                winBet = true;
                playerScript.AdjustMoney(pot * 3);//return original bet + pot doubled
                winner.SetActive(true);//display win
            }
            else
            {
                loseBet = true;
                loser.SetActive(true);//display lost
            }
        }
        //banker wins money
        else if (GameObject.Find("Player").GetComponent<PlayerScript>().handValue < GameObject.Find("Banker").GetComponent<PlayerScript>().handValue)
        {
            if (bankerWins == true)
            {
                winBet = true;
                playerScript.AdjustMoney(pot * 3);//return original bet + pot doubled
                winner.SetActive(true);//display win
            }
            else
            {
                loseBet = true;
                loser.SetActive(true);//display lost
            }
        }
        //tie wins money
        else if (GameObject.Find("Player").GetComponent<PlayerScript>().handValue == GameObject.Find("Banker").GetComponent<PlayerScript>().handValue)
        {
            if (tieWins == true)
            {
                winBet = true;
                playerScript.AdjustMoney(pot * 10);//return original bet + 9 times win ratio
                winner.SetActive(true);//display win
            }
            else
            {
                loseBet = true;
                loser.SetActive(true);//display lost
            }
        }
        StartCoroutine(RoundEnd());
    }
    IEnumerator RoundEnd()
    {

        yield return new WaitForSeconds(5);
        EndRound();
    }
    public void EndRound()
    {
        roundOver = true;
        if (roundOver)
        {
            dealBtn.gameObject.SetActive(true);
            betBtn.gameObject.SetActive(true);
            buttonGroup1.SetActive(false);
            buttonGroup2.SetActive(false);
            cashText.text = "$" + playerScript.GetMoney().ToString();
            exitBtn.gameObject.SetActive(true);
            loser.SetActive(false);
            winner.SetActive(false);
            playerHandValues.SetActive(false);
            bankerHandValues.SetActive(false);
            pot = 0;
            betOnText.text = "Bet on:";

        }
        playerScript.ResetHand();
        bankerScript.ResetHand();
    }

    private void ExitApp()
    {
        Application.Quit();
        Debug.Log("exited");
    }
}

