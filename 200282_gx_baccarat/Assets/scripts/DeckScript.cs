﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq.Expressions;

public class DeckScript : MonoBehaviour
{
    public Sprite[] cardSprites; //card prefabs in "deck"
    int[] cardValues = new int[53]; //how many cards
    int currentIndex = 0;


    void Start()
    {
        GetCardValues();
    }

    void GetCardValues()
    {
        int num = 0;
        // Loop for assigning values to the cards
        for (int i = 0; i < cardSprites.Length; i++)
        {
            num = i;
            // Count up to the amount of cards, 52
            num %= 13;
            // if there is a remainder after x/13, then the remainder is used as the value
            // unless it's more than 9, then it set value to 0

            if (num > 9 || num == 0)
            {
                num = 0;
                Debug.Log("remainder is more than 9, and has been set to 0");
            }
            cardValues[i] = num++;
            if(num == 0)
            {
                cardValues[i] = 0;
            }
        }


    }

    public void Shuffle()
    {
        //data swapping for shuffle
        for (int i = cardSprites.Length - 1; i > 0; --i)
        {
            //returns random number from deck as j 
            int j = Mathf.FloorToInt(Random.Range(0.0f, 1.0f) * cardSprites.Length - 1) + 1;
            //i = 52 (no. of prefab cards in deck); 
            Sprite face = cardSprites[i];
            //replace card number (i) with value returned (j)
            cardSprites[i] = cardSprites[j];
            //use number from j for corresponding card number
            cardSprites[j] = face;

            int value = cardValues[i];
            cardValues[i] = cardValues[j];
            cardValues[j] = value;
        }

        currentIndex = 0;
        Debug.Log("deck has been shuffled");
    }
    public int DealCard(CardScript cardScript)
    {
        cardScript.SetSprite(cardSprites[currentIndex]);
        cardScript.SetValue(cardValues[currentIndex]);
        currentIndex++;//add 1 to index
        return cardScript.GetValueOfCard();//get value of card in player/banker hand
    }
    public Sprite GetCardBack()
    {
        return cardSprites[0];
    }



}
