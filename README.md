# 200282_Nikita Naidoo_GX201 _Baccarat

**Baccarat Systems**

**Betting**
Players must first select bet. This reveals the bet amount options and what to bet on(either the player winning, banker winning, or if it will be a tie). Bet amount is added from cash total.
![alt text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/betAmount.png)

Regardless of if a bet amount is used, the player cannot deal without selecting what is being bet on;
![alt text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/betOn.png)

**Card Values**
card values are determined by there index number before being shuffled.
any number that has a value of more than 9 after being divided by 13, is set to 0.
![alt text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/getCardValues.png)


**Shuffle**
Cards in the list are randomized before dealing.
![alt text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/shuffle.png)


**Dealing**
When dealing, both the player and banker get 2 cards.
![alt  text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/dealCard1.png)
![alt text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/dealFunction.png)

It checks, if the requirements are met, a 3rd card is dealt for the respective person.
![alt text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/get3rdCard.png)

If third card is dealt, it is calculated with the other 2 cards in hand, if no 3rd card is added then just the 2 cards are calculated.
The calculated result is displayed next to and for each hand.

**Bet result**
If the bet(banker, tie, player) that was made matches the end result from the hands dealt, then the player is paid out according to the win ratios.
![alt  text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/winningCalculation.png)

**EndRound**
End of round is automatically started.
Game resets while keeping current cash amount.
![alt  text](https://gitlab.com/student73/200282_nikita-naidoo-_baccarat/-/blob/master/readMe%20pics/resetHand.png)

